vim.keymap.set("n", "<C-j>", "}")
vim.keymap.set("n", "<C-k>", "{")

vim.cmd([[xnoremap p P]])

-- Completion

-- vim.keymap.set("i", "<tab>", "pumvisible() ? <CR> : <Tab>", { expr = true, noremap = true, silent = true })

-- Window

vim.keymap.set("n", "<leader>z", "<C-w>")

-- Tab

vim.keymap.set("n", "<leader>t", ":tabnew<cr>")
vim.keymap.set("n", "<leader>tc", ":tabclose<cr>")
vim.keymap.set("n", "<leader>tl", ":tabn<cr>")
vim.keymap.set("n", "<leader>th", ":tabp<cr>")

-- Spell

vim.keymap.set("n", "<leader>sp", ":set spell<cr>")
vim.keymap.set("n", "<leader>spf", function()
	vim.opt.spell = true
	vim.opt.spelllang = "fr"
end)
vim.keymap.set("n", "<leader>spe", function()
	vim.opt.spell = true
	vim.opt.spelllang = "en"
end)

-- LSP

vim.keymap.set("n", "<leader>dk", vim.diagnostic.goto_prev, { desc = "Go to previous [D]iagnostic message" })
vim.keymap.set("n", "<leader>dj", vim.diagnostic.goto_next, { desc = "Go to next [D]iagnostic message" })

-- Copilot

vim.cmd([[imap <silent><script><expr> <C-J> copilot#Accept("\<CR>")]])
vim.cmd([[imap <silent><script><expr> <C-K> copilot#Next()]])
vim.cmd([[let g:copilot_no_tab_map = v:true]])

-- Lazygit

vim.keymap.set("n", "<leader>gl", ":LazyGit<cr>", { desc = "Open [L]azy [G]it" })

-- todo-comment

vim.keymap.set("n", "<leader>tt", ":TodoTelescope<cr>", { desc = "Open [T]odo [T]elescope" })
vim.keymap.set("n", "<leader>tll", ":TodoLocList<cr>", { desc = "Open [T]odo [L]ocation [L]ist" })
