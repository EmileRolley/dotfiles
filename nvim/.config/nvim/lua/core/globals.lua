vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = false
vim.opt.smarttab = true
vim.opt.smartindent = true

vim.opt.wrap = false
vim.opt.smartcase = true
vim.opt.colorcolumn = "90"

vim.opt.shortmess = {
	o = true,
}

-- NOTE: This is a workaround for the issue with the formatexpr (gq)
-- vim.cmd([[]])
vim.cmd([[
set formatexpr=
set textwidth=0
set wrapmargin=0
set formatoptions=tcqn
]])

-- Status line stick to the bottom
vim.opt.laststatus = 3

-- Hide the command bar when not needed
vim.opt.showcmd = false
vim.opt.cmdheight = 0
vim.api.nvim_create_autocmd("RecordingEnter", {
	desc = "Show the command line",
	callback = function()
		vim.opt.cmdheight = 1
	end,
})
vim.api.nvim_create_autocmd("RecordingLeave", {
	desc = "Hide the command line",
	callback = function()
		vim.opt.cmdheight = 0
	end,
})

vim.opt.mouse = "a"

vim.opt.showmode = false

-- Save undo history
vim.opt.undofile = true

-- Case-insensitive searching UNLESS \C or capital in search
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Keep signcolumn on by default
vim.opt.signcolumn = "yes"

-- Decrease update time
vim.opt.updatetime = 50
vim.opt.timeoutlen = 300

-- Configure how new splits should be opened
vim.opt.splitright = true
vim.opt.splitbelow = true

-- Preview substitutions live, as you type!
vim.opt.inccommand = "split"

-- Set highlight on search, but clear on pressing <Esc> in normal mode
vim.opt.hlsearch = true
vim.keymap.set("n", "<Esc>", "<cmd>nohlsearch<CR>")

vim.opt.scrolloff = 8

vim.filetype.add({
	extension = {
		publicodes = "publicodes",
	},
})

-- vim.cmd(string.format([[highlight DiffAdd cterm=bold gui=bold guibg=%s]], "#233524"))
-- vim.cmd(string.format([[highlight DiffChange cterm=bold gui=bold guibg=%s]], "#273142"))
-- vim.cmd(string.format([[highlight DiffDelete cterm=bold gui=bold guibg=%s]], "#422741"))
