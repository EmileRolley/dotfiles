return {
	"mhinz/vim-startify",
	lazy = false,
	keys = {
		{ "<leader>pq", ":SClose<cr>", { mode = "n" } },
	},
}
