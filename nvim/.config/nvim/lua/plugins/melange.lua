return {
	"savq/melange-nvim",
	lazy = false, -- make sure we load this during startup
	priority = 1000, -- make sure to load this before all the other start plugins
	config = function()
		vim.opt.termguicolors = true
		vim.cmd.colorscheme("melange")
	end,
}
