return {
	"stevearc/oil.nvim",
	opts = {},
	-- Optional dependencies
	dependencies = { "nvim-tree/nvim-web-devicons" },
	keys = {
		{ "fp", "<cmd>Oil<cr>", mode = { "n" } },
	},
	config = function()
		require("oil").setup({
			default_file_explorer = true,
			-- The default configuration
			-- You can use this to override the default configuration
			-- See the documentation for a list of available options
		})
	end,
}
