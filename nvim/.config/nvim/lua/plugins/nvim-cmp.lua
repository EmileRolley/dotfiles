return {
	"hrsh7th/nvim-cmp",
	event = { "InsertEnter", "CmdlineEnter" },
	dependencies = {
		{
			"L3MON4D3/LuaSnip",
			build = (function()
				-- Build Step is needed for regex support in snippets
				-- This step is not supported in many windows environments
				-- Remove the below condition to re-enable on windows
				if vim.fn.has("win32") == 1 or vim.fn.executable("make") == 0 then
					return
				end
				return "make install_jsregexp"
			end)(),
		},
		"saadparwaiz1/cmp_luasnip",
		"hrsh7th/cmp-nvim-lsp",
		"hrsh7th/cmp-path",
		"onsails/lspkind-nvim",
		"roobert/tailwindcss-colorizer-cmp.nvim",
	},
	config = function()
		local lspkind = require("lspkind")
		lspkind.init()

		local luasnip = require("luasnip")
		luasnip.config.setup({})

		local cmp = require("cmp")

		-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
		cmp.setup.cmdline({ "/", "?" }, {
			mapping = cmp.mapping.preset.cmdline(),
			sources = {
				{ name = "buffer" },
			},
		})

		-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
		cmp.setup.cmdline(":", {
			mapping = cmp.mapping.preset.cmdline(),
			completion = { completeopt = "menu,menuone,noinsert" },
			sources = cmp.config.sources({
				{ name = "cmdline" },
				{ name = "path" },
			}),
		})

		cmp.setup({
			snippet = {
				expand = function(args)
					luasnip.lsp_expand(args.body)
				end,
			},
			mapping = cmp.mapping.preset.insert({
				["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
				["<C-y>"] = cmp.config.disable,
				["<tab>"] = cmp.mapping(
					cmp.mapping.confirm({
						behavior = cmp.ConfirmBehavior.Insert,
						select = true,
					}),
					{ "i", "c" }
				),

				-- ["<Tab>"] = cmp.mapping(function(fallback)
				-- 	-- This little snippet will confirm with tab, and if no entry is selected, will confirm the first item
				-- 	if cmp.visible() then
				-- 		local entry = cmp.get_selected_entry()
				-- 		if not entry then
				-- 			cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
				-- 		end
				-- 		cmp.confirm()
				-- 	else
				-- 		fallback()
				-- 	end
				-- end, { "i", "s", "c" }),
			}),
			completion = { completeopt = "menu,menuone,noinsert" },
			sources = cmp.config.sources({
				{ name = "nvim_lsp" },
				{ name = "path" },
				{ name = "buffer", keyword_length = 3 },
			}),
			formatting = {
				format = lspkind.cmp_format({
					mode = "symbol",
					menu = {
						buffer = "B",
						nvim_lsp = "L",
						path = "P",
					},
				}),
			},
			experimental = {
				native_menu = false,
				ghost_text = false,
			},
		})

		cmp.config.formatting = {
			format = require("tailwindcss-colorizer-cmp").formatter,
		}
	end,
}
