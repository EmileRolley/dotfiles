return {
	"nvim-treesitter/nvim-treesitter",
	dependencies = {
		"rescript-lang/tree-sitter-rescript",
	},
	build = ":TSUpdate",
	config = function()
		-- [[ Configure Treesitter ]] See `:help nvim-treesitter`

		local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
		parser_config.rescript = {
			install_info = {
				url = "https://github.com/rescript-lang/tree-sitter-rescript",
				branch = "main",
				files = { "src/scanner.c" },
				generate_requires_npm = false,
				requires_generate_from_grammar = true,
				use_makefile = true, -- macOS specific instruction
			},
		}
		---@diagnostic disable-next-line: missing-fields
		require("nvim-treesitter.configs").setup({
			ensure_installed = {
				"bash",
				"c",
				"html",
				"lua",
				"markdown",
				"vim",
				"vimdoc",
				"elm",
				"ocaml",
				"typescript",
				"javascript",
				"css",
				"rescript",
			},
			-- Autoinstall languages that are not installed
			auto_install = true,
			highlight = { enable = true },
			indent = { enable = true },
		})

		-- There are additional nvim-treesitter modules that you can use to interact
		-- with nvim-treesitter. You should go explore a few and see what interests you:
		--
		--    - Incremental selection: Included, see `:help nvim-treesitter-incremental-selection-mod`
		--    - Show your current context: https://github.com/nvim-treesitter/nvim-treesitter-context
		--    - Treesitter + textobjects: https://github.com/nvim-treesitter/nvim-treesitter-textobjects
	end,
}
