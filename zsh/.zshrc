# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/emile/.oh-my-zsh"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.opam/default/bin/ocamlmerlin:$PATH"
export PATH_TO_FX="/home/emile/Jars/javafx-sdk-14/lib"
export PG_OF_PATH="/home/emile/bin/of_v0.11.0_linux64gcc6_release"

export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border --prompt="fzf > "'
export FZF_DEFAULT_COMMAND='find'
export OUNIT_CI=true

export PATH="$HOME/.emacs.d/bin:$PATH"
export PATH="$HOME/node_modules/.bin/:$PATH"
export PATH="/home/emile/lsp/eclipse.jdt.ls/:$PATH"
export PATH=$PATH:$HOME/.local/bin

export JAR="/home/emile/lsp/eclipse.jdt.ls/org.eclipse.jdt.ls.product/target/repository/plugins/org.eclipse.equinox.launcher_1.6.0.v20200915-1508.jar"
export JAVA_HOME="/usr/lib/jvm/java-14-openjdk-amd64"
# export JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64"
export GRADLE_HOME="$HOME/gradle"
export JDTLS_CONFIG="/home/emile/.local/bin/jdtls"
export WORKSPACE="/home/emile/eclipse-workspace"

export MERMAID_BIN="/home/emile/.local/bin/pandoc-mermaid"

export PATH="/home/emile/zls:$PATH"

export PATH="/home/emile/bin/ltex-ls-15.2.0/bin:$PATH"

export PATH="/usr/local/texlive/2022/bin/x86_64-linux:$PATH"

export PATH="/home/emile/.rbenv/bin:$PATH"

export DEEPL_API_KEY="ed1e25ca-614e-855a-c56a-fcc340df6b95"

export SCALINGO_MONGO_URL="SCALINGO_MONGO_URL=mongodb://nosgestesclimat-7372:VmEPYACJhm2h1fvO90wu@28531de5-6fca-48de-ad40-bacce287092a.nosgestesclimat-7372.mongo.dbs.scalingo.com:36907/nosgestesclimat-7372?replicaSet=nosgestesclimat-7372-rs0&ssl=true"

export DENO_INSTALL="/home/emile/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"

export PATH=$PATH:./node_modules/.bin
# export DEEPL_API_KEY="37ee9e01-9b46-6c4e-dc53-fb5632be2f7b"
#
export NODE_OPTIONS="--max_old_space_size=4096"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes

ZSH_THEME="zhann"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  bun
  extract
  fzf-tab
  git
  history
  npm
  sudo
  vi-mode
  web-search
  yarn
  z
  # zsh-syntax-highlighting
  zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#

#### APT ####

alias aptin="sudo apt install"
alias aptup="sudo apt update"
alias aptug="sudo apt upgrade"
alias update="sudo apt update && apt upgrade"

#### File Manager / Code editor ###
#
alias vi="/home/emile/bin/nvim.appimage"
# alias vi="nvim"
# alias nvim="/home/emile/bin/nvim.appimage -u ~/.config/nvim/minimal.vim"
alias vim="/home/emile/bin/nvim.appimage -u ~/.config/nvim/init.vim"
alias vis="/home/emile/bin/nvim.appimage -u ~/.config/nvim/init.vim -S session.vim"

function nvim_update() {
	curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
	chmod u+x nvim.appimage
	mv nvim.appimage /home/emile/bin/nvim.appimage
}

alias e="emacs"

alias lf="vifm ."
alias lr="ranger"
alias codec="code ."

alias ls="exa"
alias l="ls -l"
alias ll="ls -l"
alias la="ls -la"

alias rm="rm --preserve-root"

alias diff="difftastic"

#### Dev ####

alias python="python3"
alias p3="python3"
alias m="make"
alias mr="make run"

function proc() {
	/home/emile/bin/processing-3.5.4/processing-java
}

function proca() {
	/home/emile/bin/processing-3.5.4/processing-java --sketch="$1" --run
}

#### Git ####

alias lg="lazygit"

#### Watson ####

alias task="watson"

#### Bin ####

alias staruml="./bin/StarUML-3.1.1.AppImage &"
alias tutamail="./bin/tutanota-desktop-linux.AppImage &"
alias dcord="./bin/6cord/6cord -t \"NTAwOTc0ODE3MDQ3MDg1MDc2.XejlSw.UGwzjh5vIu8kF4Y5i5Haa10fDiQ\""
alias spt="spotifyd -d spotify-daemon && spt"

alias bat="bat --theme ansi-dark"

#### Ltex-ls ####

alias ltex-ls="/home/emile/bin/ltex-ls-15.2.0/bin/ltex-ls"

#### Projects ####

alias makegen="~/Projects/makegen/makegen.sh"

source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#### Publicodes ####

alias publicomp="/home/emile/Projects/datagir/publicodes/packages/optimizer/bin/publicomp"

# opam configuration
test -r /home/emile/.opam/opam-init/init.zsh && . /home/emile/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

#### Documentation ####

function ch() {
	curl cheat.sh/"$1"
}

[ -f "/home/emile/.ghcup/env" ] && source "/home/emile/.ghcup/env" # ghcup-env

#### Bluetooth ####

function bton() {
	bluetoothctl power on
	bluetoothctl connect 88:C6:26:CD:4F:21
}

function btoff() {
	bluetoothctl power off
}

export PATH=$PATH:/home/emile/.spicetify

# Commented out because it was consecently slowing down the shell (60% of the startup time)
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# bun completions
[ -s "/home/emile/.bun/_bun" ] && source "/home/emile/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"

# pnpm
export PNPM_HOME="/home/emile/.local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end

alias bundle="bundle3.0"

eval "$(starship init zsh)"
